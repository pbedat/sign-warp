import React from "react";
import ReactDOM from "react-dom";
import App from "./app";

ReactDOM.render(React.createElement(App), document.querySelector("#app"));

// const server = new EventSource("http://localhost:8080/host/pbedat.de");
