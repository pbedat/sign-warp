import React, { useState, useEffect, useCallback } from "react";

const App: React.FC = () => {
  const [isRegistering, setIsRegistering] = useState(false);
  const [email, setEmail] = useState("");
  const [authEmail, setAuthEmail] = useState("");
  const [auth, setAuth] = useState<{ token: string; name: string }>();
  const [waitingForVerification, setWaitingForVerification] = useState(false);

  useEffect(() => {
    if (Notification.permission !== "granted") {
      Notification.requestPermission();
    }
  }, []);

  useEffect(() => {
    if (!isRegistering) {
      return;
    }

    const events = new EventSource(
      //`http://localhost:8080/register/pbedat.de/${email}`
      `https://sign-warp.pbedat.de/register/pbedat.de/${email}`
    );
    events.onerror = err => {
      console.warn(err);
    };
    events.addEventListener("email", (msg: any) => setAuthEmail(msg.data));
    events.addEventListener("auth", (msg: any) => {
      new Notification("You are now registered!", { vibrate: 100 });
      setAuth(JSON.parse(msg.data));
    });

    return () => events.close();
  }, [isRegistering]);

  const signIn = useCallback(() => {
    const events = new EventSource(
      //`http://localhost:8080/register/pbedat.de`
      `https://sign-warp.pbedat.de/register/pbedat.de`
    );
    events.onerror = err => {
      console.warn(err);
      events.close();
    };
    events.addEventListener("email", (msg: any) => {
      setEmail("woop");
      setWaitingForVerification(true);
      setIsRegistering(true);
      setAuthEmail(msg.data);
      window.open(
        `mailto:${msg.data}?subject=Send this email to register&body=...nothing else required`
      );
    });
    events.addEventListener("auth", (msg: any) => {
      new Notification("You are now registered!", { vibrate: 100 });
      setAuth(JSON.parse(msg.data));
    });
  }, []);

  return (
    <div>
      {!isRegistering && (
        <>
          <h1>Sick of confirming registrations in your emails?</h1>
          <h2>Don' worry - we'll fix this!</h2>
          <p>
            <input
              placeholder="E-Mail"
              type="email"
              value={email}
              onChange={ev => setEmail(ev.target.value)}
            />
          </p>
          <p>
            <button onClick={() => setIsRegistering(true)}>Register</button>
          </p>
          <button onClick={signIn}>Sign in</button>
        </>
      )}

      {authEmail && !auth && (
        <div>
          <h1>Is this really you?</h1>
          <p>Prove it!</p>
          <p>
            <a
              className="button"
              onClick={() => setWaitingForVerification(true)}
              target="_blank"
              href={`mailto:${authEmail}?body=...nothing else requried&subject=Just send the mail`}
            >
              Verify your email
            </a>
          </p>
          {waitingForVerification && <p>Waiting for your email...</p>}
        </div>
      )}

      {auth && (
        <>
          <h1>Thank you!</h1>
          <p>
            <input type="text" value={auth.name} />
          </p>
          <button>Complete your registration</button>
        </>
      )}
    </div>
  );
};

export default App;
