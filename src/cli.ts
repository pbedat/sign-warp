import yargs from "yargs";
import runServer from "./server";

const { port } = yargs.option("port", { default: 8080, type: "number" }).argv;

runServer(port);
