import { Router } from "express";
import createService from "./service";

function createApi() {
  const api = Router();

  const service = createService();

  api.get("/", (_, res) => res.send("sign-warp"));

  api.get("/host/:domain", (req, res) => {
    res.contentType("text/event-stream");
    res.write("event: HELLO\ndata: sign-warp\n\n");

    const [err, host] = service.hostRegistration(req.params.domain);

    if (!host) {
      return res.status(400).json(err);
    }

    host.on("auth", auth => {
      res.write(`data: ${JSON.stringify(auth)}\n\n`);
    });

    req.on("close", () => {
      host.close();
    });
  });

  api.get("/register/:domain/:email?", (req, res) => {
    res.contentType("text/event-stream");

    const [err, registration] = service.createRegistration({
      domain: req.params.domain,
      email: req.params.email
    });

    if (!registration) {
      res.status(400).json(err);
      return;
    }

    registration.on("init", email => {
      res.write(`event: email\ndata: ${email}\n\n`);
    });

    registration.on("register", auth => {
      res.write(`event: auth\ndata: ${JSON.stringify(auth)}\n\n`);
    });

    registration.on("timeout", () => {
      res.end();
    });

    req.on("close", registration.close);
  });

  return api;
}

export default createApi;
