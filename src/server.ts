import express from "express";
import cors from "cors";
import api from "./api";
import createBeacon from "pbedat-dashboard-middleware";

function runServer(port = 8080) {
  const app = express();

  app.use(cors());

  const { beacon } = createBeacon({
    serviceId: "sign-warp",
    streamBinInstanceUrl: "https://streambin.pbedat.de"
  });

  app.use(beacon);
  app.use(api());

  app.listen(port, () =>
    process.stdout.write(`listening to http://127.0.0.1:${port}\n`)
  );
}

export default runServer;
