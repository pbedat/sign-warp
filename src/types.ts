export type Either<TError, TResult> =
  | [TError]
  | [TError, undefined]
  | [undefined, TResult];
