import { omit, partial } from "lodash";
import uuid = require("uuid");
import { EventEmitter } from "events";
import EventSource from "eventsource";
import { Either } from "./types";

type State = Record<string, RegistrationHost>;

interface Registration {
  token: string;
  address: string;
  name: string;
}

interface RegistrationHost {
  readonly domain: string;
  close(): void;
  on(type: "close", fn: () => void): void;
  on(type: "auth", handler: (auth: Registration) => void): void;
  register(registration: Registration): void;
}

function hostRegistration(
  store: {
    dispatch: (updateFn: (state: State) => State) => void;
    getState(): State;
  },
  domain: string
): Either<RegistrationError, RegistrationHost> {
  const { dispatch, getState } = store;

  if (getState()[domain]) {
    return [
      {
        error: "HOST_ALREADY_EXISTS",
        message: `domain "${domain}" is already being hosted`
      }
    ];
  }

  const events = new EventEmitter();

  const host: RegistrationHost = {
    domain,
    close() {
      events.removeAllListeners();
      dispatch((registrationHosts: State) => omit(registrationHosts, domain));
    },
    on(type: "close" | "auth", handler: any) {
      events.on(type, handler);
    },
    register(payload: Registration) {
      events.emit("auth", payload);
    }
  };

  dispatch((registrationHosts: State) => ({
    ...registrationHosts,
    [domain]: host
  }));

  return [, host];
}

interface RegistrationError {
  readonly error: string;
  readonly message: string;
}

interface RegistrationClient {
  close(): void;

  on(type: "init", handler: (email: string) => void): void;
  on(type: "register", handler: (registration: Registration) => void): void;
  on(type: "timeout", handler: () => void): void;
  on(type: "error", handler: (error: RegistrationError) => void): void;
}

function createRegistration(
  getHost: (domain: string) => RegistrationHost,
  request: { domain: string; email?: string }
): Either<RegistrationError, RegistrationClient> {
  const { domain, email: from } = request;

  const emitter = new EventEmitter();

  const host = getHost(domain);

  if (!host) {
    return [
      {
        error: "HOST_OFFLINE",
        message: `no host waiting for registrations on domain "${domain}"`
      },
      undefined
    ];
  }

  const mailbox = uuid();

  const events = new EventSource(
    `https://mail-fu.pbedat.de/api/${domain}/${mailbox}/events`
  );

  events.onopen = () => emitter.emit("init", `${mailbox}@${domain}`);

  setTimeout(() => {
    emitter.emit("timeout");
    events.close();
  }, 5 * 60 * 1000);

  events.onmessage = (msg: MessageEvent) => {
    const email = JSON.parse(msg.data);

    const token = uuid();

    if (!from || email.from.value[0].address === from) {
      const { name, address } = email.from.value[0];
      getHost(domain).register({ token, address, name });
      emitter.emit("register", { token, address, name });
      events.close();
    }
  };

  return [
    ,
    {
      on(type: string, handler: any) {
        emitter.on(type, handler);
      },
      close() {
        emitter.removeAllListeners();
        events.close();
      }
    }
  ];
}

export default function createService() {
  let state: State = {};

  return {
    hostRegistration: partial(hostRegistration, {
      dispatch: update => (state = update(state)),
      getState() {
        return state;
      }
    }),
    createRegistration: partial(
      createRegistration,
      (domain: string) => state[domain]
    )
  };
}
