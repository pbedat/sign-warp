# sign-warp

```
your server ----- ( listen for registrations ) ----> sign-warp

your client ----- ( user wants to register) ----> sign warp


user           client        server          sign-warp
  | -register->  |             |                 |
  |             [ ] ----start registration----->[ ]
  |             [ ] <---send random email addr--[ ]
  |-click link->[ ]            |                [ ]
  |----send mail------------------------------->[ ]
  |             [ ]            |<--token+email--[ ]
                [ ]<--------send token----------[ ]
```
